package com.example.parking.app

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class appRetrofit {
    var retrofit: Retrofit? = null
    fun getClient(baseUrl: String?): Retrofit? {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit
    }

    enum class type {
        PARKING,
        NO_PARKING
    }

}


