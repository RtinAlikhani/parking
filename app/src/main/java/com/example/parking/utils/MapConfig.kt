package com.example.parking.utils

import android.app.Application
import com.example.parking.R
import ir.map.sdk_map.Mapir
import ir.map.servicesdk.MapirService

class MapConfig : Application() {
    override fun onCreate() {
        super.onCreate()

        MapirService.init(baseContext, getString(R.string.api_key))
        Mapir.getInstance(baseContext, getString(R.string.api_key));

    }
}