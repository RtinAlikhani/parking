package com.example.parking.api.model

import com.example.parking.app.appRetrofit

data class Type (
    val type:Enum<appRetrofit.type>
)

