package com.example.parking.api.model

data class mLatLng(
    val Lat: Double,
    val Lng: Double,
    val Id: Int,
    val Type: Int,
    val IsActive: Boolean
)