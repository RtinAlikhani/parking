package com.example.parking.api

import com.example.parking.api.model.Parking
import com.example.parking.app.appRetrofit
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface Api {
    @GET("GetParking")
    fun parkings(): Call<Parking>

    @FormUrlEncoded
    @POST("PostLocation")
    fun addNewParking(
        @Field("lat") lat: Double,
        @Field("Lng") lng: Double,
        @Field("Type") type: Int
    ): Call<ResponseBody>

    @GET("GetDeleteLocation")
    fun deleteParking(
        @Query("id") id:Int
    ): Call<ResponseBody>

}