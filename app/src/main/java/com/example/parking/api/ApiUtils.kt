package com.example.parking.api

import com.example.parking.app.appRetrofit

class ApiUtils {

    val BASE_URL = "http://server.allgram.ir/api/Parking/"
    fun getInstance(): Api {
        return appRetrofit().getClient(BASE_URL)!!.create(Api::class.java)

    }
}