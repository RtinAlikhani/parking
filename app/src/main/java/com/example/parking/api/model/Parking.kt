package com.example.parking.api.model

data class Parking(
    val Parking: List<mLatLng>,
    val NoParking: List<mLatLng>
)