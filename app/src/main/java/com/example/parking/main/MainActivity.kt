package com.example.parking.main

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.parking.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import java.util.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    var gps_enabled = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermesion()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: Fragment? = null
        when (item.itemId) {
            R.id.direction -> {
                fragment = DirectionFragment()
            }
            R.id.parking -> {
                fragment = ParkingFragment()
            }
            R.id.noParking -> {
                fragment = NotParkingFragment()
            }
        }

        if (fragment != null) {
            val transaction =
                supportFragmentManager.beginTransaction()
            if (supportFragmentManager.fragments.isEmpty()) {
                transaction.add(
                    R.id.container,
                    fragment,
                    String.format(Locale.US, "item: %d", item.itemId)
                ).commit()
            } else {
                transaction.replace(
                    R.id.container,
                    fragment,
                    String.format(Locale.US, "item: %d", item.itemId)
                ).commit()
                invalidateOptionsMenu()
            }
            return true
        }

        return false
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        val isGranted = PermissionsManager.areLocationPermissionsGranted(this)
        if(isGranted)
            checkGps()
        else{
            Toast.makeText(
                this@MainActivity,
                "برای موقعیت یابی به این مجور نیاز هست.",
                Toast.LENGTH_LONG
            ).show()
            checkPermesion()
        }

        val allFragments: List<*> = supportFragmentManager.fragments
        if (allFragments.isEmpty()) {
            return
        }
        val currentFragment =
            allFragments[allFragments.size - 1] as Fragment
        if (currentFragment is PermissionsListener) {
            currentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun checkGps() {
        val lm: LocationManager =
            this.getSystemService(Context.LOCATION_SERVICE) as (LocationManager)
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {
        }

        if (!gps_enabled) {
            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setMessage("برای استفاده از این برنامه باید مکان یاب شما فعال باشد")
                .setCancelable(false)
                .setPositiveButton("فعال کردن") { _, _ ->
                    this.startActivityIfNeeded(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 85)
                }
            val alert = dialogBuilder.create()
            alert.setTitle("اخظار")
            alert.show()
        }

        if (gps_enabled) {
                val navigation = findViewById<BottomNavigationView>(R.id.navigationView)
                navigation.setOnNavigationItemSelectedListener(this@MainActivity)
                navigation.selectedItemId = R.id.direction
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 85)
            checkGps()
    }

    fun checkPermesion() {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            checkGps()
        } else {
            val permissionsManager =
                PermissionsManager(object : PermissionsListener {
                    override fun onExplanationNeeded(permissionsToExplain: List<String>) {}
                    override fun onPermissionResult(granted: Boolean) {
                        if (granted) {
                            checkGps()
                        }else {
                            Toast.makeText(
                                this@MainActivity,
                                "برای موقعیت یابی به این مجور نیاز هست.",
                                Toast.LENGTH_LONG
                            ).show()
                            checkPermesion()
                        }
                    }
                })
            permissionsManager.requestLocationPermissions(this)
        }
    }


}
