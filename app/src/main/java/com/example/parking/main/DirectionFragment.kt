package com.example.parking.main

import android.app.Dialog
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.parking.R
import com.example.parking.api.ApiUtils
import com.example.parking.api.model.Parking
import com.example.parking.api.model.mLatLng
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.geojson.LineString
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.*
import com.mapbox.mapboxsdk.style.layers.Property.ICON_ROTATION_ALIGNMENT_VIEWPORT
import ir.map.sdk_map.MapirStyle
import ir.map.servicesdk.MapService
import ir.map.servicesdk.ResponseListener
import ir.map.servicesdk.enums.RouteType
import ir.map.servicesdk.model.base.MapirError
import ir.map.servicesdk.request.RouteRequest
import ir.map.servicesdk.response.RouteResponse
import kotlinx.android.synthetic.main.fragment_direction.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.absoluteValue

class DirectionFragment : Fragment() {

    var best: Int = 0
    var worst: Int = 1

    var vbToTraffic = 9
    var vbToDest = 1
    var vbToSource = 6

    var vwToTraffic = 3
    var vwToDest = 9
    var vwToSource = 1

    lateinit var map: MapboxMap
    var mapStyle: Style? = null

    var bfilters = listOf(vbToTraffic, vbToDest, vbToSource)
    var wfilters = listOf(vwToTraffic, vwToDest, vwToSource)

    var sourceLatitude: Double? = null
    var sourceLongitude: Double? = null
    var destinationLongitude: Double? = null
    var destinationLatitude: Double? = null

    var listParkingToDest = ArrayList<Double>()
    var listSourceToParking = ArrayList<Double>()
    var listDuration = ArrayList<Double>()
    var listNearlyParking = ArrayList<mLatLng>()
    var listParking = ArrayList<mLatLng>()

    var nTable = Array(100) { DoubleArray(4) }
    var modelTable = Array(100) { DoubleArray(2) }
    lateinit var mapView: MapView
    val mapService: MapService = MapService()
    val VANAK_SQUARE = LatLng(35.7572, 51.4099)
    var destState = false
    var gps_enabled = false

    val api = ApiUtils().getInstance()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_direction, container, false)
    }


    private fun enableLocationComponent(view: View) {
        val context = view.context
            val customLocationComponentOptions =
                LocationComponentOptions.builder(context)
                    .elevation(5f)
                    .accuracyAlpha(0.6f)
                    .accuracyColor(R.color.colorPrimaryLight)
                    .build()


            val locationComponent = map.locationComponent
            val locationComponentActivationOptions =
                LocationComponentActivationOptions.builder(context, mapStyle!!)
                    .locationComponentOptions(customLocationComponentOptions)
                    .build()

            locationComponent.activateLocationComponent(locationComponentActivationOptions)
            locationComponent.isLocationComponentEnabled = true
            locationComponent.cameraMode = CameraMode.TRACKING_COMPASS
            locationComponent.renderMode = RenderMode.COMPASS
            val location = LatLng(
                locationComponent.lastKnownLocation!!.latitude,
                locationComponent.lastKnownLocation!!.longitude
            )
            val zoom = 19
            map.easeCamera(CameraUpdateFactory.newLatLngZoom(location, zoom.toDouble()))

            sourceLatitude = locationComponent.lastKnownLocation?.latitude
            sourceLongitude = locationComponent.lastKnownLocation?.longitude

            val editor: SharedPreferences.Editor =
                requireActivity().getSharedPreferences("location", MODE_PRIVATE).edit()
            editor.putString("lat", "$sourceLatitude")
            editor.putString("lng", "$sourceLongitude")
            editor.apply()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView = view.findViewById(R.id.map_view)
        mapView.onCreate(savedInstanceState)
        initMap()
        mapView.addOnDidFinishLoadingMapListener {
            fab_location.visibility = View.VISIBLE
            pb_map.visibility = View.GONE
           try {
               enableLocationComponent(map_view)
           }catch (e:java.lang.Exception) {
                map.easeCamera(CameraUpdateFactory.newLatLngZoom(VANAK_SQUARE, 15.toDouble()))
            }
        }

        getParkings()
        getNoParkings()
        fab_location.setOnClickListener { enableLocationComponent(map_view) }
        iv_filter.setOnClickListener { changeFilter() }
    }

    private fun initMap() {
        mapView.getMapAsync(OnMapReadyCallback { mapboxMap ->
            map = mapboxMap
            map.setStyle(
                Style.Builder()
                    .fromUri(MapirStyle.MAIN_MOBILE_VECTOR_STYLE)
            ) { style ->
                mapStyle = style

            }

            map.addOnMapLongClickListener { point ->
                checkDestClick(point)
                false
            }
        })
    }

    fun checkDestClick(point: LatLng) {
        if (destState) {
            val dialog = Dialog(requireContext())
            dialog.setContentView(R.layout.layout_dialog_confirm)
            val message = dialog.findViewById<TextView>(R.id.tv_message)
            val title = dialog.findViewById<TextView>(R.id.tv_title)
            message.text = "ایا میخواهید مقصد جدید انتخاب کنید؟"
            title.text = "تغییر مقصد"
            val yesBtn = dialog.findViewById(R.id.btn_yes) as Button
            val noBtn = dialog.findViewById(R.id.btn_cancel) as TextView
            yesBtn.setOnClickListener {
                dialog.show()
                mapStyle!!.removeImage("ic_destination")
                destState = false
                val ft = fragmentManager!!.beginTransaction()
                ft.detach(this).attach(this).commit()
                dialog.dismiss()
            }
            noBtn.setOnClickListener { dialog.dismiss() }
            dialog.show()
        } else {
            addSymbolToMap(point, 1)
        }
    }

    private fun addSymbolToMap(point: LatLng, symbolState: Int) {
        if (mapStyle != null) {
            try {

                val sampleSymbolManager = SymbolManager(mapView, map, mapStyle!!)
                var iconName = "ic_new_parking"
                when (symbolState) {
                    1 -> {
                        ContextCompat.getDrawable(requireContext(), R.drawable.ic_destination)
                            ?.let {
                                mapStyle!!.addImage(
                                    "ic_destination",
                                    it
                                )
                            }
                        iconName = "ic_destination"
                        destState = true
                    }
                    2 -> {
                        ContextCompat.getDrawable(requireContext(), R.drawable.ic_parking_a)?.let {
                            mapStyle!!.addImage(
                                "ic_parking_a",
                                it
                            )
                        }
                        iconName = "ic_parking_a"
                    }
                    3 -> {
                        ContextCompat.getDrawable(requireContext(), R.drawable.ic_no_parking)?.let {
                            mapStyle!!.addImage(
                                "ic_no_parking",
                                it
                            )
                        }
                        iconName = "ic_no_parking"
                    }
                }


                sampleSymbolManager.iconAllowOverlap = true
                sampleSymbolManager.iconRotationAlignment = ICON_ROTATION_ALIGNMENT_VIEWPORT

                val sampleSymbolOptions = SymbolOptions()
                sampleSymbolOptions.withLatLng(point)
                sampleSymbolOptions.withIconImage(iconName)
                sampleSymbolOptions.withIconSize(1.5f)

                val sampleSymbol: Symbol = sampleSymbolManager.create(sampleSymbolOptions)

                if (symbolState == 1) {
                    destinationLatitude = sampleSymbol.geometry.latitude()
                    destinationLongitude = sampleSymbol.geometry.longitude()

                    fillTable(
                        sourceLatitude!!,
                        sourceLongitude!!,
                        destinationLatitude!!,
                        destinationLongitude!!
                    )
                }
            }catch (e:Exception){
                Log.e("error","$e")
            }
        }

    }

    private fun getParkings() {
        api.parkings().enqueue(object : Callback<Parking> {

            override fun onResponse(
                call: Call<Parking>?,
                response: Response<Parking>?
            ) {
                for (i in response?.body()!!.Parking.indices) {
                    val result = response.body()!!
                    val parking = LatLng(result.Parking[i].Lat, result.Parking[i].Lng)

                    listParking.add(response.body()!!.Parking[i])
                    addSymbolToMap(parking, 2)
                }
            }

            override fun onFailure(call: Call<Parking>?, t: Throwable?) {
                Toast.makeText(requireContext(), "خطا در ارتباط با سرور", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    private fun getNoParkings() {
        api.parkings().enqueue(object : Callback<Parking> {

            override fun onResponse(
                call: Call<Parking>?,
                response: Response<Parking>?
            ) {
                for (i in response?.body()!!.NoParking.indices) {
                    val result = response.body()!!
                    val parking = LatLng(result.NoParking[i].Lat, result.NoParking[i].Lng)
                    addSymbolToMap(parking, 3)
                }
            }

            override fun onFailure(call: Call<Parking>?, t: Throwable?) {
                Toast.makeText(requireContext(), "خطا در ارتباط با سرور", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    private fun fillTable(
        sourceLatitude: Double,
        sourceLongitude: Double,
        destinationLatitude: Double,
        destinationLongitude: Double
    ) {
        val location = LatLng(sourceLatitude, sourceLongitude)
        val destination = LatLng(destinationLatitude, destinationLongitude)
        for (i in 0 until listParking.size) {

            val parking = LatLng(listParking[i].Lat, listParking[i].Lng)
            val distance = parking.distanceTo(destination)
            if (distance < 4000) {
                listNearlyParking.add(
                    mLatLng(
                        listParking[i].Lat,
                        listParking[i].Lng,
                        listParking[i].Id,
                        listParking[i].Type,
                        listParking[i].IsActive
                    )
                )
                listParkingToDest.add(parking.distanceTo(destination))
                listSourceToParking.add(location.distanceTo(parking))
                getDuration(
                    sourceLatitude,
                    sourceLongitude,
                    listParking[i].Lat,
                    listParking[i].Lng
                )
            }

        }
    }

    private fun convertToNormalTable() {
        for (j in 0 until listNearlyParking.size) {
            nTable[j][0] = j.toDouble()
            nTable[j][1] = (listDuration[j] - (listDuration.min()!!)) /
                    (listDuration.max()!! - listDuration.min()!!)
            nTable[j][2] = (listParkingToDest[j] - (listParkingToDest.min()!!)) /
                    (listParkingToDest.max()!! - listParkingToDest.min()!!)
            nTable[j][3] = (listSourceToParking[j] - (listSourceToParking.min()!!)) /
                    (listSourceToParking.max()!! - listSourceToParking.min()!!)
        }
        findBestParking()
    }

    private fun findBestParking() {
        for (j in 0 until listNearlyParking.size) {
            var bRes = 0.0
            var wRes = 0.0
            modelTable[j][0] = listNearlyParking[j].Id.toDouble()
            for (i in 1..3) {
                modelTable[j][1] += nTable[j][best].minus(bfilters[i - 1].times(nTable[j][i])).absoluteValue +
                        (nTable[j][i].minus(wfilters[i - 1].times(nTable[j][worst])).absoluteValue)

            }
        }
        var max = 0.0
        var index = 0
        for (i in 0 until listNearlyParking.size) {
            if (modelTable[i][1] > max) {
                max = modelTable[i][1]
                index = i
            }
        }
        router(
            sourceLatitude!!,
            sourceLongitude!!,
            listNearlyParking[index].Lat,
            listNearlyParking[index].Lng,
            index
        )
    }

    fun getDuration(
        startLatitude: Double,
        startLongitude: Double,
        endLatitude: Double,
        endLongitude: Double
    ) {
        val requestBody = RouteRequest.Companion.Builder(
            startLatitude, startLongitude,
            endLatitude, endLongitude,
            RouteType.DRIVING
        ).build()
        mapService.route(requestBody, object : ResponseListener<RouteResponse> {
            override fun onSuccess(response: RouteResponse) {
                listDuration.add(response.routes[0].weight)
                if (listNearlyParking.size != 0) {
                    if (listDuration.size == listNearlyParking.size)
                        convertToNormalTable()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "پارکینگی در نزدیکی این محدوده ثبت نشده است",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

            }

            override fun onError(error: MapirError) {
                Toast.makeText(requireContext(), "مشکلی در مسیریابی پیش آمده", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    private fun router(
        startLatitude: Double,
        startLongitude: Double,
        endLatitude: Double,
        endLongitude: Double,
        index: Int
    ) {
        val requestBody = RouteRequest.Companion.Builder(
            startLatitude, startLongitude,
            endLatitude, endLongitude,
            RouteType.DRIVING
        ).build()
        mapService.route(requestBody, object : ResponseListener<RouteResponse> {
            override fun onSuccess(response: RouteResponse) {
                showRouteOnMap(response.routes[0].geometry, index)
                Toast.makeText(requireContext(), "پاسخ مسیریابی دریافت شد", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onError(error: MapirError) {
                Toast.makeText(requireContext(), "مشکلی در مسیریابی پیش آمده", Toast.LENGTH_SHORT)
                    .show()
            }
        })


    }

    fun deleteParking(id: Int) {
        api.deleteParking(id).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(
                    requireContext(),
                    "مشکلی در انتخاب پارکینگ پیش آمده",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                Toast.makeText(requireContext(), "پارکینگ مناسب انتخاب شد", Toast.LENGTH_SHORT)
                    .show()
            }

        })
    }

    fun showRouteOnMap(geometry: String, index: Int) {
        val lineManager = LineManager(mapView, map, mapStyle!!, "hw-secondary-tertiary")
        val routeLine = LineString.fromPolyline(geometry, 5)
        val lineOptions = LineOptions()
            .withGeometry(routeLine)
            .withLineColor("#ff5252")
            .withLineWidth(5f)
        lineManager.create(lineOptions)
        deleteParking(listNearlyParking[index].Id)
    }


    private fun changeFilter() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.layout_dialog_filter)

        val btnNext1 = dialog.findViewById(R.id.btn_next1) as Button
        val btnNext2 = dialog.findViewById(R.id.btn_next2) as Button
        val btnNext3 = dialog.findViewById(R.id.btn_next3) as Button
        val btnBack2 = dialog.findViewById(R.id.btn_back2) as Button
        val btnBack3 = dialog.findViewById(R.id.btn_back3) as Button

        val rgBest = dialog.findViewById(R.id.radioGroup1) as RadioGroup
        val rgWorst = dialog.findViewById(R.id.radioGroup2) as RadioGroup

        val rbwTraffic = dialog.findViewById(R.id.rb_w_traffic) as RadioButton
        val rbwDestenation = dialog.findViewById(R.id.rb_w_pToDestenation) as RadioButton
        val rbwSource = dialog.findViewById(R.id.rb_w_sToParking) as RadioButton

        val etBTraffic = dialog.findViewById(R.id.et_b_traffic) as EditText
        val etBDestenation = dialog.findViewById(R.id.et_b_pToDestenation) as EditText
        val etBSource = dialog.findViewById(R.id.et_b_sToParking) as EditText
        val etwTraffic = dialog.findViewById(R.id.et_w_traffic) as EditText
        val etwDestenation = dialog.findViewById(R.id.et_w_pToDestenation) as EditText
        val etwSource = dialog.findViewById(R.id.et_w_sToParking) as EditText

        val layout_best = dialog.findViewById(R.id.layout_best) as ConstraintLayout
        val layout_worst = dialog.findViewById(R.id.layout_worst) as ConstraintLayout
        val layout_values = dialog.findViewById(R.id.layout_values) as ConstraintLayout


        btnNext1.setOnClickListener {
            when (rgBest.checkedRadioButtonId) {
                R.id.rb_b_traffic -> {
                    best = 0
                    rbwTraffic.visibility = View.GONE
                    etBTraffic.setText("1")
                    etBTraffic.isEnabled = false
                }
                R.id.rb_b_pToDestenation -> {
                    best = 1
                    rbwDestenation.visibility = View.GONE
                    etBDestenation.setText("1")
                    etBDestenation.isEnabled = false
                }
                R.id.rb_b_sToParking -> {
                    best = 2
                    rbwSource.visibility = View.GONE
                    etBSource.setText("1")
                    etBSource.isEnabled = false
                }
            }
            layout_best.visibility = View.GONE
            layout_worst.visibility = View.VISIBLE
        }

        btnNext2.setOnClickListener {
            when (rgWorst.checkedRadioButtonId) {
                R.id.rb_w_traffic -> {
                    worst = 0
                    etwTraffic.setText("1")
                    etwTraffic.isEnabled = false
                }
                R.id.rb_w_pToDestenation -> {
                    worst = 1
                    etwDestenation.setText("1")
                    etwDestenation.isEnabled = false
                }
                R.id.rb_w_sToParking -> {
                    worst = 2
                    etwSource.setText("1")
                    etwSource.isEnabled = false
                }
            }
            layout_worst.visibility = View.GONE
            layout_values.visibility = View.VISIBLE
        }

        btnNext3.setOnClickListener {
            if (etBTraffic.text.isNotEmpty() && etBDestenation.text.isNotEmpty() &&
                etBSource.text.isNotEmpty() && etwTraffic.text.isNotEmpty() && etwDestenation.text.isNotEmpty()
                && etwSource.text.isNotEmpty()
            ) {
                vbToTraffic = Integer.parseInt(etBTraffic.text.toString())
                vbToDest = Integer.parseInt(etBDestenation.text.toString())
                vbToSource = Integer.parseInt(etBSource.text.toString())
                vwToTraffic = Integer.parseInt(etwTraffic.text.toString())
                vwToDest = Integer.parseInt(etwDestenation.text.toString())
                vwToSource = Integer.parseInt(etwSource.text.toString())

                bfilters = listOf(vbToTraffic, vbToDest, vbToSource)
                wfilters = listOf(vwToTraffic, vwToDest, vwToSource)

                dialog.dismiss()
            } else {
                Toast.makeText(requireContext(), "مقادیر نمیتوانند خالی باشند", Toast.LENGTH_LONG)
                    .show()
            }
        }

        btnBack2.setOnClickListener {
            layout_best.visibility=View.VISIBLE
            layout_worst.visibility=View.GONE
            rbwTraffic.visibility=View.VISIBLE
            rbwDestenation.visibility=View.VISIBLE
            rbwSource.visibility=View.VISIBLE
            etwTraffic.setText("")
            etwTraffic.isEnabled = true
            etwDestenation.setText("")
            etwDestenation.isEnabled = true
            etwSource.setText("")
            etwSource.isEnabled = true

            etBTraffic.setText("")
            etBTraffic.isEnabled = true
            etBDestenation.setText("")
            etBDestenation.isEnabled = true
            etBSource.setText("")
            etBSource.isEnabled = true
        }

        btnBack3.setOnClickListener {
            etwTraffic.setText("")
            etwTraffic.isEnabled = true
            etwDestenation.setText("")
            etwDestenation.isEnabled = true
            etwSource.setText("")
            etwSource.isEnabled = true

            layout_values.visibility=View.GONE
            layout_worst.visibility=View.VISIBLE
        }
        dialog.show()
    }


    override fun onStart() {
        super.onStart()
        map_view.onStart()
    }

    override fun onResume() {
        super.onResume()
        map_view.onResume()
    }

    override fun onPause() {
        super.onPause()
        map_view.onPause()
    }

    override fun onStop() {
        super.onStop();
        map_view.onStop();
    }

    override fun onLowMemory() {
        super.onLowMemory();
        map_view.onLowMemory();
    }

    override fun onDestroyView() {
        super.onDestroyView()
        map_view.onDestroy()
    }

}