package com.example.parking.main

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.parking.R
import com.example.parking.api.ApiUtils
import com.example.parking.api.model.Parking
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import com.mapbox.mapboxsdk.style.layers.Property
import ir.map.sdk_map.MapirStyle
import ir.map.servicesdk.MapService
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_direction.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.concurrent.schedule

class NotParkingFragment : Fragment() {
    var map: MapboxMap? = null
    var mapStyle: Style? = null
    lateinit var mapView: MapView
    val VANAK_SQUARE = LatLng(35.7572, 51.4099)
    val api = ApiUtils().getInstance()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_not_parking, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapView = view.findViewById(R.id.map_view)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(OnMapReadyCallback { mapboxMap ->
            map = mapboxMap
            map!!.setStyle(
                Style.Builder()
                    .fromUri(MapirStyle.MAIN_MOBILE_VECTOR_STYLE)
            ) { style ->
                mapStyle = style
            }
            mapView.addOnDidFinishLoadingMapListener {
                fab_location.visibility = View.VISIBLE
                pb_map.visibility = View.GONE
              try {
                  enableLocationComponent(map_view)
              }catch (e:Exception) {
                    map!!.easeCamera(CameraUpdateFactory.newLatLngZoom(VANAK_SQUARE, 15.toDouble()))
                }
            }
            map!!.addOnMapLongClickListener { point ->
                confirmAddParking(point)
                false
            }
        })
        val timer = Timer("schedule", true)
        timer.schedule(2000) {
            getParkings()
            getNoParkings()
        }
        fab_location.setOnClickListener { enableLocationComponent(map_view) }
    }

    private fun enableLocationComponent(view: View) {
        val context = view.context
            val customLocationComponentOptions =
                LocationComponentOptions.builder(context)
                    .elevation(5f)
                    .accuracyAlpha(.6f)
                    .accuracyColor(R.color.colorPrimaryLight)
                    .build()

            val locationComponent = map!!.locationComponent
            val locationComponentActivationOptions =
                LocationComponentActivationOptions.builder(context, mapStyle!!)
                    .locationComponentOptions(customLocationComponentOptions)
                    .build()

            locationComponent.activateLocationComponent(locationComponentActivationOptions)
            locationComponent.isLocationComponentEnabled = true
            locationComponent.cameraMode = CameraMode.TRACKING
            locationComponent.renderMode = RenderMode.COMPASS

            val location = LatLng(
                locationComponent.lastKnownLocation!!.latitude,
                locationComponent.lastKnownLocation!!.longitude
            )
            val zoom = 19
            map!!.easeCamera(CameraUpdateFactory.newLatLngZoom(location, zoom.toDouble()))
    }

    private fun confirmAddParking(point: LatLng) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.layout_dialog_confirm)
        val message = dialog.findViewById<TextView>(R.id.tv_message)
        val title = dialog.findViewById<TextView>(R.id.tv_title)
        val icon = dialog.findViewById<ImageView>(R.id.icon)
        icon.setImageDrawable(resources.getDrawable(R.drawable.ic_no_parking))
        message.text = "آیا از افزودن پارک ممنوع در این نقطه اطمینان دارید؟"
        title.text = "تایید افزودن پارک ممنوع"
        val yesBtn = dialog.findViewById(R.id.btn_yes) as Button
        val noBtn = dialog.findViewById(R.id.btn_cancel) as TextView
        yesBtn.setOnClickListener {
            addNewParking(point)
            dialog.dismiss()
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    private fun addNewParking(point: LatLng) {
        api.addNewParking(
            point.latitude,
            point.longitude,
            2
        ).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                TODO("Not yet implemented")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                addSymbolToMap(point,true,true)
            }


        })
    }


    private fun addSymbolToMap(point: LatLng, isNotParking: Boolean, isNew: Boolean) {
        if (mapStyle != null) {
            try {

                var iconName = "ic_new_parking"
                if (isNotParking) {
                    if (isNew) {
                        ContextCompat.getDrawable(requireContext(), R.drawable.ic_new_no_parking)
                            ?.let {
                                mapStyle!!.addImage(
                                    "ic_new_no_parking",
                                    it
                                )
                            }
                        iconName = "ic_new_no_parking"
                    } else {
                        ContextCompat.getDrawable(requireContext(), R.drawable.ic_no_parking)?.let {
                            mapStyle!!.addImage(
                                "ic_no_parking",
                                it
                            )
                        }
                        iconName = "ic_no_parking"
                    }
                } else {
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_parking_a)?.let {
                        mapStyle!!.addImage(
                            "ic_parking_a",
                            it
                        )
                    }
                    iconName = "ic_parking_a"
                }

                val sampleSymbolManager = SymbolManager(mapView, map!!, mapStyle!!)
                sampleSymbolManager.iconAllowOverlap = true
                sampleSymbolManager.iconRotationAlignment =
                    Property.ICON_ROTATION_ALIGNMENT_VIEWPORT
                val sampleSymbolOptions = SymbolOptions()
                sampleSymbolOptions.withLatLng(point)
                sampleSymbolOptions.withIconImage(iconName)
                sampleSymbolOptions.withIconSize(1.5f)
                val sampleSymbol: Symbol = sampleSymbolManager.create(sampleSymbolOptions)
            }catch (e:Exception){
                Log.e("error","$e")
            }
        }
    }

    private fun getParkings() {
        api.parkings().enqueue(object : Callback<Parking> {

            override fun onResponse(
                call: Call<Parking>?,
                response: Response<Parking>?
            ) {
                for (i in response?.body()!!.Parking.indices) {
                    val result = response.body()!!
                    val parking = LatLng(result.Parking[i].Lat, result.Parking[i].Lng)
                    addSymbolToMap(parking, false, false)
                }
            }

            override fun onFailure(call: Call<Parking>?, t: Throwable?) {
                Toast.makeText(requireContext(), "خطا در ارتباط با سرور", Toast.LENGTH_SHORT)
                    .show()
            }
        })

    }

    private fun getNoParkings() {
        api.parkings().enqueue(object : Callback<Parking> {

            override fun onResponse(
                call: Call<Parking>?,
                response: Response<Parking>?
            ) {
                for (i in response?.body()!!.NoParking.indices) {
                    val result = response.body()!!
                    val parking = LatLng(result.NoParking[i].Lat, result.NoParking[i].Lng)
                    addSymbolToMap(parking, true, false)
                }
            }

            override fun onFailure(call: Call<Parking>?, t: Throwable?) {
                Toast.makeText(requireContext(), "خطا در ارتباط با سرور", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    override fun onStart() {
        super.onStart()
        map_view.onStart()
    }

    override fun onResume() {
        super.onResume()
        map_view.onResume()
    }

    override fun onPause() {
        super.onPause()
        map_view.onPause()
    }

    override fun onStop() {
        super.onStop()
        map_view.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map_view.onLowMemory()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        map_view.onDestroy()
    }
}